const axios = require('axios')

exports.handler = (event, context, callback) => {
    const { 
        apiKey, 
        base, 
        key, 
        playlist, 
        video,
    } = process.env
    
    const playlistId = event.query.playlistid
    const playlistUrl = `${base}${playlist}${playlistId}${key}${apiKey}`

    let tracks = []

    let getTracks = async(url, pageToken) => {
        if(pageToken)
            url += `&pageToken=${pageToken}`
            
        try {
            const dataResponse = await axios.get(url, pageToken)
            const trackItems = dataResponse.data.items
            
            await Promise.all(trackItems.map(async (item) => {
                const videoId = item.snippet.resourceId.videoId
                let uploader = null
                
                const uploaderResponse = await axios.get(`${base}${video}${videoId}${key}${apiKey}`)
                if (uploaderResponse.data.items.length > 0) {
                    uploader = uploaderResponse.data.items[0].snippet.channelTitle
                }
                
                const data = {
                    id: item.id,
                    channelId: item.snippet.channelId,
                    channelTitle: uploader,
                    title: item.snippet.title,
                    data: item.snippet,
                    details: item.contentDetails,
                }
    
                tracks = [data, ...tracks]
            }))
            
            const nextPageToken = dataResponse.data.nextPageToken
        
            if(nextPageToken) {
                getTracks(playlistUrl, nextPageToken)
            }
            else {
                const sorted = tracks.sort((a, b) => {
                    if (a.data) {
                        return a.data.position - b.data.position
                    }
                    
                    return 1
                })
                
                callback(null, sorted)
            }
        } catch(error) {
            callback(error, null)
        }
    }
    
    getTracks(playlistUrl, '')
}